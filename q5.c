#include <stdio.h>

int main()
{
	int A = 10,B = 15;
	
	printf("Using And Operator: %d\n",A&B);
	printf("Using XOR operator : %d\n",A^B);
	printf("Using Complement : %d\n",~A);
	printf("Using left shift : %d\n",A<<3);
	printf("Using right shift : %d\n",B>>3);
	return 0;
	
}
